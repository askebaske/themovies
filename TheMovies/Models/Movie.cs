﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMovies.Models
{
	public class Movie
	{
		public string? Title { get; set; }
		public int Duration { get; set; }
		public DateOnly? ReleaseDate { get; set; }
		public string? Genre { get; set; }
		public string? Director { get; set; }
		public string? PrimaryGenre 
		{ 
			get
			{
				return Genre?.Split(',')[0];
			}
		}
	}
}

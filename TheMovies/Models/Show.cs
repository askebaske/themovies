﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TheMovies.Models
{
	public class Show
	{
		public Movie Movie { get; set; }
        public DateOnly ShowDate { get; set; }
        public TimeOnly ShowTime { get; set; }
        public string ShowDuration { get; set; }
		public string ShowEndTime { get; set; }

		public Show(Movie movie, DateOnly showDate, TimeOnly showTime)
		{
			Movie = movie;
			ShowDate = showDate;
			ShowTime = showTime;
			ShowDuration = CalculateDuration(movie);
			ShowEndTime = CalculateEndTime(movie);
		}

		private string CalculateEndTime(Movie movie)
		{
			return new DateTime(ShowDate.Year, ShowDate.Month, ShowDate.Day, ShowTime.Hour, ShowTime.Minute, 0).AddHours((movie.Duration+30)/60).AddMinutes((movie.Duration+30)%60).ToString("t");
		}

		private string CalculateDuration(Movie movie)
		{
			return $"{(movie.Duration+30)/60} Timer, {(movie.Duration+30)%60} Minutter";
		}
    }
}

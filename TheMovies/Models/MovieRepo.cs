﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TheMovies.Models
{
	public class MovieRepo
	{
		private string connectionString = "Server=10.56.8.36; Database=DB_F23_TEAM_15; User Id=DB_F23_TEAM_15; Password=TEAMDB_DB_15; TrustServerCertificate=true";

		private List<Movie> movies = new();

		public MovieRepo() 
		{
			RetrieveAllFromDB();
        }

		public void Add(Movie movie) 
		{ 
			movies.Add(movie);
		}

		public void Delete(Movie movie) 
		{ 
			movies.Remove(movie);
		}

		//public void Update(Movie movie) 
		//{ 
			
		//}

		public List<Movie> GetAll()
		{
			return new List<Movie>(movies.OrderBy(Movie => Movie.Title));
		}

		public void RetrieveAllFromDB()
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				con.Open();
				SqlCommand cmd = new SqlCommand("Select * from movie", con);

				using (SqlDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						Movie movie = new Movie();

						movie.Title = reader["Title"].ToString();
						bool i = int.TryParse(reader["Duration"].ToString(), out int duration);
						if (i)
							movie.Duration = duration;
						movie.Genre = reader["Genre"].ToString();
					
						movies.Add(movie);
					}
				}
			}
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMovies.Models
{
	public class Cinema
	{
        public string? Name { get; set; }
        public List<Hall>? Halls { get; set; }

        public Cinema(string name, string[] id)
        {
            Name = name;
            Halls = new List<Hall>();
            foreach (string i in id)
                Halls.Add(new Hall(i));
        }

        // eksempel
        //Cinema Århus = new("Århus Bio", new[] { 1, 2, 3, 4 });
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using TheMovies.ViewModels;

namespace TheMovies.Models
{
	public class Hall
	{
        public string ID { get; set; }
        public ObservableCollection<Show>? Shows { get; set; }

        public Hall(string id)
        {
            ID = id;
            InitializeCollection();
        }

        private void InitializeCollection()
        {
            Shows = new ObservableCollection<Show>();
            var grouping = new PropertyGroupDescription
            {
                PropertyName = nameof(Show.ShowDate)
            };
            CollectionViewSource.GetDefaultView(Shows).GroupDescriptions.Add(grouping);
            CollectionViewSource.GetDefaultView(Shows).SortDescriptions.Add(new SortDescription("ShowDate", ListSortDirection.Ascending));
            CollectionViewSource.GetDefaultView(Shows).SortDescriptions.Add(new SortDescription("ShowTime", ListSortDirection.Ascending));
            CollectionViewSource.GetDefaultView(Shows).Filter = new Predicate<object>(DateRange);
        }

        private bool DateRange(object show)
        {
            return ((Show)show).ShowDate >= DateOnly.FromDateTime(DateTime.Today);
        }
    }
}

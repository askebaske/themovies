﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TheMovies.Models;
using TheMovies.ViewModels;

namespace TheMovies.Commands
{
	public class DeleteMovieCommand : CommandBase
	{
		public override void Execute(object? parameter)
		{
			if (parameter is MainViewModel mvm && mvm.SelectedMovie != null)
			{
				mvm.Movies?.Remove(mvm.SelectedMovie);
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using TheMovies.ViewModels;

namespace TheMovies.Commands
{
	public class GroupByGenreCommand : CommandBase
	{
		public override void Execute(object? parameter)
		{
			if (parameter is MainViewModel mvm)
			{
				mvm.GroupByGenre();
			}
		}
	}
}

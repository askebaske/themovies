﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TheMovies.Models;
using TheMovies.ViewModels;
using TheMovies.Views;

namespace TheMovies.Commands
{
	public class NewMovieCommand : CommandBase
	{
		public override void Execute(object? parameter)
		{
			NewMovieDialog newMovieDialog = new();
			if (parameter is MainViewModel mvm)
			{
				Movie newMovie = new();
				mvm.SelectedMovie = newMovie;
				newMovieDialog.DataContext = mvm;
				newMovieDialog.ShowDialog();
				if (newMovieDialog.DialogResult == true)
				{
					mvm.Movies?.Add(mvm.SelectedMovie);
					mvm.MovieView?.Refresh();
				}
			}
		}
	}
}

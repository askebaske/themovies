﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using TheMovies.ViewModels;

namespace TheMovies.Commands
{
	public class NewShowCommand : CommandBase
	{
		public override void Execute(object? parameter)
		{
			if (parameter is MainViewModel mvm)
			{
				if (mvm.SelectedMovie == null)
					MessageBox.Show("Vælg en film fra listen", "Ingen film valgt!", MessageBoxButton.OK, MessageBoxImage.Warning);
				else
					mvm.NewShow();
			}
		}
	}
}

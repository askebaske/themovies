﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TheMovies.Models;
using TheMovies.Commands;
using CommunityToolkit.Mvvm.ComponentModel;
using System.Windows.Data;
using System.ComponentModel;
using System.Globalization;

namespace TheMovies.ViewModels
{
	public partial class MainViewModel : ObservableObject
	{
		//--------Constructor------------------------------------------
		//-------------------------------------------------------------
		public MainViewModel() => InitializeCollections();

		//-----------Lists---------------------------------------------
		//-------------------------------------------------------------
		public ObservableCollection<Cinema>? Cinemas { get; set; } = new ObservableCollection<Cinema>();
		public ObservableCollection<Movie>? Movies;
		public ICollectionView? MovieView { get; set; }

        //-------------Fields------------------------------------------
        //-------------------------------------------------------------
		private readonly MovieRepo _movieRepo = new();

        [ObservableProperty]
		private Movie? selectedMovie;
		[ObservableProperty]
		private Hall? selectedHall;
		[ObservableProperty]
		private Cinema? selectedCinema;
		[ObservableProperty]
		private string? hour;
		[ObservableProperty]
		private string? minute;

		private string? _nameSearchString;
		private DateTime _date = DateTime.Now;

		//------------Properties----------------------------------------
		//--------------------------------------------------------------
		public int[] Hours { get; set; } = Enumerable.Range(0, 23).ToArray();
		public int[] Minutes { get; set; } = Enumerable.Range(0, 59).Where(x => x % 5 == 0).ToArray();

		public string? NameSearchString
		{
			get => _nameSearchString;
			set 
			{ 
				_nameSearchString = value;
				MovieView?.Refresh();	
			}
		}
		public DateTime Date
		{
			get => _date;
			set
			{
				_date = value;
				SetProperty(ref _date, value);
			}
		}

		//---------Commands---------------------------------------------
		//--------------------------------------------------------------
		public ICommand DeleteMovieCmd { get; set; } = new DeleteMovieCommand();
		public ICommand NewMovieCmd { get; set; } = new NewMovieCommand();
		public ICommand GroupByGenreCmd { get; set; } = new GroupByGenreCommand();
		public ICommand NewShowCmd { get; set; } = new NewShowCommand();

		//---------Methods---------------------------------------------
		//-------------------------------------------------------------
		public void NewShow()
		{
			Show newShow = new(SelectedMovie, DateOnly.FromDateTime(_date), TimeOnly.Parse($"{Hour}:{Minute}"));
			SelectedCinema?.Halls?[int.Parse(SelectedHall.ID)-1].Shows?.Add(newShow);
		}

		public void InsertMovie()
		{
			Movie newMovie = new()
			{
				Title = SelectedMovie?.Title,
				Duration = SelectedMovie?.Duration ?? 60,
				Genre = SelectedMovie?.Genre
			};

			Movies?.Add(newMovie);
		}

		private void InitializeCollections()
		{
			Movies = new ObservableCollection<Movie>(_movieRepo.GetAll());
			MovieView = CollectionViewSource.GetDefaultView(Movies);
			MovieView.Filter = new Predicate<object>(Contains);

			Cinema Århus = new("Århus Bio", new[] { "1", "2", "3" });
			Cinema Odense = new("Nordisk Film Odense", new[] { "1", "2" });
			Cinema Ikast = new("Ikast Bio", new[] { "1", "2", "3", "4" });
			Cinemas?.Add(Århus);
			Cinemas?.Add(Odense);
			Cinemas?.Add(Ikast);
			Show show1 = new(new Movie { Title = "Matrix", Genre = "Documentary", Duration = 125 }, DateOnly.Parse("2023-10-16"), TimeOnly.Parse("20:00"));
			Show show2 = new(new Movie { Title = "Schindlers Fist", Genre = "Comedy", Duration = 184 }, DateOnly.Parse("2023-10-16"), TimeOnly.Parse("19:00"));
			Århus.Halls?[0].Shows?.Add(show1);
			Ikast.Halls?[1].Shows?.Add(show2);
		}

		private bool Contains(object o)
		{
			if (_nameSearchString != null)
			{
				return ((Movie)o).Title?.ToLower().Contains(_nameSearchString.ToLower()) ?? false;
			}
			return true;
		}

		public void GroupByGenre()
		{
			if (MovieView?.GroupDescriptions.Count > 0)
			{
				MovieView.GroupDescriptions.Clear();
				MovieView.SortDescriptions.Clear();
			}
			else
			{
				var grouping = new PropertyGroupDescription
				{
					PropertyName = nameof(Movie.PrimaryGenre)
				};
				MovieView?.GroupDescriptions.Add(grouping);
				MovieView?.SortDescriptions.Add(new SortDescription("PrimaryGenre", ListSortDirection.Ascending));
				MovieView?.SortDescriptions.Add(new SortDescription("Title", ListSortDirection.Ascending));
			}

			MovieView?.Refresh();
		}
	}
}

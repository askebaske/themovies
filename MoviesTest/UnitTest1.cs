using TheMovies.Models;
using TheMovies.ViewModels;

namespace MoviesTest
{
	[TestClass]
	public class UnitTest1
	{
		MainViewModel mvm;

		[TestInitialize]
		public void Init()
		{
			mvm = new();

			mvm.SelectedMovie.Title = "Test";
			mvm.SelectedMovie.Duration = 100;
			mvm.SelectedMovie.Genre = "TestGenre";
		}

		//[TestMethod]
		//public void TestMovieList()
		//{
		//	Assert.AreEqual("Rocky V", mvm.Movies[0].Title);
		//}

		[TestMethod]
		public void TestInsertMovie()
		{
			mvm.InsertMovie();
			Assert.AreEqual("Test", mvm.Movies.Last().Title);
			Assert.AreEqual(100, mvm.Movies.Last().Duration);
			Assert.AreEqual("TestGenre", mvm.Movies.Last().Genre);
		}
	}
}